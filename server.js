var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var router_model = require('./src/Router');

var server = express();
server.use(express.json());
server.use(bodyParser.urlencoded({ extended: true }));

router_model.register_router(server);

http.createServer(server).listen(9555, ()=>{
    console.log("Server started successfully on port 9555");
});