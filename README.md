# Kaam24-login

Tech Stack:

	- Node.js
	- Express.js
	- MongoDB


Steps to deploy the application:

	- First make sure latest version of node, npm and mongodb should be present.
	- clone the project at a particular directory.
	- move inside the projet directory and run the following commands:
		- `npm install` it will install all node modules.
		- `node server.js` it will start the server.
	- default server port is 9555, you can change it at 'server.js' file.


Descriptions:

	- Mainly application contains 5 rest api end point.

	1. POST: '/signup' for registering new user.
		post request should contain the json data like: {"u_name": "subhash", "u_email": "subhash.negi.909@gmail.com", "u_pass": "123456", "u_contact": "987456321"}
	
	2. POST: '/login' for login.
		post request should contain the json data like: { "u_email": "subhash.negi.909@gmail.com", "u_pass": "123456"}
	
	3. POST: '/update' for updating user details.
		For calling this api you need session token in cookie 't_token' which you will get after login.
		post request should contain the json data  which you want to update like: {"u_name": "subhash singh", "u_contact": "987456321"}
	
	4. POST: '/send_m' for sending message to other user.
		For calling this api you need session token in cookie 't_token' which you will get after login.
		post request should contain the json data like: {"message": "Hello", "r_email": "subhash.negi@gmail.com"}
	
	5. GET: '/read_m' for fetch unread messages.
		For calling this api you need session token in cookie 't_token' which you will get after login.


MongoDB Collection Schemas:
	
	1. users  (Collection): to user registered users
		{
			"_id" : ObjectId("5ca07fea0581ce2cad5e9eaf"),
			"name" : "subhash",
			"email" : "subhash.negi.909@gmail.com",
			"password" : "123456",
			"contach" : "987456321",
			"time" : 1554039541006
		}
		- 'email' and '_id' should be uniq.
	
	2. messages  (Collection): to store messages
		{
			"_id" : ObjectId("5ca0c3344bcb102287614247"),
			"sender_id" : "5ca07fea0581ce2cad5e9eaf",
			"receiver_id" : "5ca0851b1230a72f51a0d7e1",
			"sender_email" : "subhash.negi.909@gmail.com",
			"receiver_email" : "subhash.negi@gmail.com",
			"user_ids" : [ "5ca07fea0581ce2cad5e9eaf", "5ca0851b1230a72f51a0d7e1" ],
			"message" : "hello",
			"time" : 1554039541006,
			"is_read" : 0
		}