const MongoClient = require('mongodb').MongoClient;

// MOngoDB process port
const conn_url = 'mongodb://localhost:27017';
// MongoDB DataBase name
const dbName = 'kaam24';
// Users collection
const user_collection = "users";
// Messages collection
const message_collection = "messages";

var db_connection;

var create_connection = (callback)=>{
    MongoClient.connect(conn_url, { useNewUrlParser: true }, (err, connection)=>{
        if(err){
            console.error("Error occurred in connectiong to MongoDB: ", err);
            callback(err);
        }else{
            db_connection = connection;
            callback && get_db_connection(callback)
        }
    });
}

var get_db_connection = (callback)=>{
    if(db_connection){
        var db_conn = db_connection.db(dbName);
        callback(undefined, db_conn);
    }else{
        create_connection(callback);
    }
}

var module_export_obj = {
   get_db_connection: get_db_connection,
   user_collection: user_collection,
   message_collection: message_collection
};
create_connection();

module.exports = module_export_obj;