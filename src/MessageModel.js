var db_connection_model = require('./DBConnection');
var mongo = require('mongodb');

var send_message = (req, res, user_obj)=>{
    var data = req.body || {};
    var message = data.message || "";
    var receiver_email = data.r_email;

    var error_message = "";
    if(!message) error_message = "Please send a valid message.";
    else if(!receiver_email) error_message = "Please provide a valid receiver email.";
    if(error_message){
        res.send({"SUCCESS": false, "ERROR_MESSAGE": error_message });
        return false;
    }
    get_user_id(receiver_email, (err, receiver_id)=>{
        if(err || !receiver_id){
            res.send({"SUCCESS": false, "ERROR_MESSAGE": "Please provide a valid receiver email." });
            return false;
        }else{
            var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
                if(err){
                    console.error("Error occurred in getting DB connection: ", err);
                    res.send({"SUCCESS": false, "ERROR_MESSAGE": "Some network error occurred." });
                }else{
                    var data_obj = {"sender_id": user_obj.id, "receiver_id": receiver_id, sender_email: user_obj.email, "receiver_email":receiver_email, user_ids: [user_obj.id, receiver_id], message: message, time: Date.now(), is_read: 0 }
                    var collection = db_connection.collection(db_connection_model.message_collection);
                    collection.insertOne(data_obj, (err, insert_result)=>{
                        if(err || insert_result.result.n==0){
                            res.send({"SUCCESS": false, "ERROR_MESSAGE": "Some network error occurred." });
                        }else{
                            res.send({"SUCCESS": true, "MESSAGE": "Message sent."});
                        }
                    });
                }
            });
        }

    });
}

var read_new_message = (req, res, user_obj)=>{
    var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
        if(err){
            console.error("Error occurred in getting DB connection: ", err);
            res.send({"SUCCESS": false, "ERROR_MESSAGE": "Some network error occurred." });
        }else{
            var data_obj = {$and: [{receiver_id: user_obj.id}, {is_read: 0}]};
            var collection = db_connection.collection(db_connection_model.message_collection);
            collection.find(data_obj).toArray((err, find_result)=>{
                if(err){
                    res.send({"SUCCESS": false, "ERROR_MESSAGE": "Some network error occurred." });
                }else{
                    res.send({"SUCCESS": true, result: find_result});
                }
            });
        }
    });

}

var get_user_id = (email, callback)=>{
    var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
        if(err){
            console.error("Error occurred in getting DB connection: ", err);
            callback(err);
        }else{
            var data_obj = {email: email};
            var collection = db_connection.collection(db_connection_model.user_collection);
            collection.find(data_obj).toArray((err, user_result)=>{
                if(err || user_result.length==0){
                    console.log("Error occurred in find query: ", err);
                    callback(err);
                }else{
                    var user_obj = JSON.parse(JSON.stringify(user_result[0]));
                    callback(undefined, user_obj._id);
                }
            });
        }
    });
}

var export_obj = {
    send_message: send_message,
    read_new_message: read_new_message
};

module.exports = export_obj;