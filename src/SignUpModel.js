var db_connection_model = require('./DBConnection');
var mongo = require('mongodb');

var sign_up = (req, res)=>{
    var req_data = req.body || {};
    var username = req_data.u_name || "";
    var user_email = req_data.u_email || "";
    var user_pass = req_data.u_pass || "";
    var user_contact = req_data.u_contact || "";
    
    var error_message = "";
    if(!username) error_message = "Please provide a valid user name";
    else if(!user_email) error_message = "Please provide a valid email id";
    else if(!user_pass) error_message = "Please provide a valid password";
    else if(!user_contact) error_message = "Please provide a valid contact number";

    if(error_message){
        res.send({"SUCCESS": false, "ERROR_TYPE": "INVALID", "ERROR_MESSAGE": error_message});
    }else{
        var data_obj = {"name": username, "email": user_email, "password": user_pass, "contact": user_contact, "create_date": Date.now()};
        var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
            if(err){
                console.error("Error occurred in getting DB connection: ", err);
                res.send({"SUCCESS": false,"ERROR_TYPE": "SERVER", "ERROR_MESSAGE": "Some network error occurred."});
            }else{
                var collection = db_connection.collection(db_connection_model.user_collection);
                collection.insertOne(data_obj, (err, insert_result)=>{
                    if(err){
                        res.send({"SUCCESS": false,"ERROR_TYPE": "DUPLICATE", "ERROR_MESSAGE": "Email ID already registered."});
                    }else{
                        console.log(JSON.stringify(insert_result));
                        res.send({"SUCCESS": true, "MESSAGE": "SignUp successful."});
                    }
                });
            }
        });
    }
}

var update_user = (req, res, user_obj)=>{
    var req_data = req.body || {};
    var username = req_data.u_name || "";
    var user_email = req_data.u_email || "";
    var user_pass = req_data.u_pass || "";
    var user_contact = req_data.u_contact || "";
    
    var data_obj = {};
    if(username) data_obj['name'] = username;
    if(user_email) data_obj['email'] = user_email;
    if(user_pass) data_obj['password'] = user_pass;
    if(user_contact) data_obj['contact'] = user_contact;

    if(Object.keys(data_obj).length==0){
        res.send({"SUCCESS": false, "ERROR_TYPE": "INVALID", "ERROR_MESSAGE": "Please provide a valid field for update."});
    }else{
        var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
            if(err){
                console.error("Error occurred in getting DB connection: ", err);
                res.send({"SUCCESS": false,"ERROR_TYPE": "SERVER", "ERROR_MESSAGE": "Some network error occurred."});
            }else{
                var update_data = { $set: data_obj };
                var collection = db_connection.collection(db_connection_model.user_collection);
                var user_id = new mongo.ObjectID(user_obj.id);
                collection.updateOne({"_id": user_id}, update_data, (err, insert_result)=>{
                    if(err || insert_result.result.n==0){
                        res.send({"SUCCESS": false,"ERROR_TYPE": "SERVER", "ERROR_MESSAGE": "Some network error occurred."});
                    }else{
                        console.log(JSON.stringify(insert_result));
                        res.send({"SUCCESS": true, "MESSAGE": "User updated successfully."});
                    }
                });
            }
        });
    }
}

var module_export_obj = {
    sign_up: sign_up,
    update_user: update_user
};

module.exports = module_export_obj;