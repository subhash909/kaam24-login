var login_signup_model = require('./SignUpModel');
var auth_model = require('./AuthModel');
var message_model = require('./MessageModel');

var register_router = function(server){

    server.route("/")
    .get((req, res)=>{
        res.end("Hello");
    });

    server.route("/signup")
    .post(login_signup_model.sign_up);

    server.route("/update")
    .post((req, res)=>{
        auth_model.check_token(req, res, login_signup_model.update_user);
    });

    server.route("/login")
    .post(auth_model.login);

    server.route("/send_m")
    .post((req, res)=>{
        auth_model.check_token(req, res, message_model.send_message);
    });

    server.route("/read_m")
    .post((req, res)=>{
        auth_model.check_token(req, res, message_model.read_new_message);
    });
}

module.exports.register_router = register_router;