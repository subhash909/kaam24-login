var db_connection_model = require("./DBConnection");
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');


var jwt_secret = "subhash.negi.909@gmail.com";
var all_tokens = {};
var user_collection = "users";

var crypt_password = (password, callback)=> {
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return callback(err);
        bcrypt.hash(password, salt, function(err, hash) {
            return callback(err, hash);
        });
    });
};

var login = (req, res)=>{
    var req_data = req.body || {};
    var user_email = req_data.u_email || "";
    var user_pass = req_data.u_pass || "";

    var error_message = "";
    if(!user_email) error_message = "Please provide a valid email id.";
    else if(!user_pass) error_message = "Please provide a valid password.";

    if(error_message){
        res.send({"SUCCESS": false, "ERROR_TYPE": "INVALID", "ERROR_MESSAGE": "Invalid email id or password."});
    }else{
        var db_connection = db_connection_model.get_db_connection((err, db_connection)=>{
            var find_query = {"email": user_email, password: user_pass};
            if(err){
                console.error("Error occurred in getting DB connection: ", err);
                res.send({"SUCCESS": false,"ERROR_TYPE": "SERVER", "ERROR_MESSAGE": "Some network error occurred."});
            }else{
                var collection = db_connection.collection(user_collection);
                collection.find(find_query).toArray((err, user_result)=>{
                    if(err || user_result.length==0){
                        res.send({"SUCCESS": false,"ERROR_TYPE": "SERVER", "ERROR_MESSAGE": "Please provide a valid email or password."});
                    }else{
                        var user_data =  JSON.parse(JSON.stringify(user_result[0]));
                        console.log(JSON.stringify(user_result));
                        var token = jwt.sign({data: {email: user_data['email'], id: user_data._id}}, jwt_secret, { expiresIn: 60*60*1 });
                        all_tokens[token] = true;
                        res.cookie('t_token', token);
                        res.send({"SUCCESS": true, "MESSAGE": "Login successful."});
                    }
                });
            }
            
        });
    }
    
}

var logout = function(req, res){
    var token = getCookie(req.headers.cookie,'t_token');
    all_tokens[token] = false;
    res.cookie('t_token', "");
    res.send({"STATUS": "SUCCESS"});
}

var check_token = (req, res, redir_method)=>{
    var token = getCookie(req.headers.cookie,'t_token');
    if(token){
        jwt.verify(token,  jwt_secret, function(err, decoded) {
            if(all_tokens[token] && decoded && decoded.data  && decoded.data.email && decoded.data.id && redir_method){
                redir_method(req, res, {email: decoded.data.email, id: decoded.data.id});
            }else{
                res.cookie('t_token', "");
                res.send({"session": false, "MESSAGE": "Invalid session"});
            }
        });
    }else{
        res.cookie('t_token', "");
        res.send({"session": false, "MESSAGE": "Invalid session"});
    }
}

function getCookie(cookie,cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var export_obj = {};
export_obj.login = login;
export_obj.check_token = check_token;
export_obj.crypt_password = crypt_password;
export_obj.logout = logout;

module.exports = export_obj;
